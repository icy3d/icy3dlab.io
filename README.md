# Getting started

- Make sure to have ruby and jekyll installed
- Execute
  ```
  jekyll build && jekyll serve
  ```
The lazy ones can simply run the script in the root folder:
- Linux: run.sh
- Win: run.bat

# Creative Theme for Jekyll

The current site was derived from this jekyll site:
- [demo of the Creative Theme](https://jekyll-themes.gitlab.io/creative/)
- [template home of the Creative Theme](http://startbootstrap.com/template-overviews/creative/)
- by [Start Bootstrap](http://startbootstrap.com).

Creative is a one page Bootstrap theme for creatives, small businesses, and other multipurpose uses.
The theme includes a number of rich features and plugins that you can use as a great boilerplate for your next Jekyll project! 

## To use the Creative Theme template in your project

- Start by adding your info in `_config.yml`
- In `_layouts/front.html` reorder or remove section as you prefer.

## Contributions

Please submit a merge request and ping @marcia for review/merge

## Configurations:

GitLab CI: [`.gitlab-ci.yml`]

Jekyll: [`_config.yml`]

Branch: [`Pages`]

----

_Forked from https://gitlab.com/steko/test/_

[`.gitlab-ci.yml`]: https://gitlab.com/jekyll-themes/creative/blob/pages/.gitlab-ci.yml
[`_config.yml`]: https://gitlab.com/jekyll-themes/creative/blob/pages/_config.yml
[`Pages`]: https://gitlab.com/jekyll-themes/creative/tree/pages
