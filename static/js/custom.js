// Expand via JavaScript:
// https://stackoverflow.com/questions/22254248/bootstrap-3-expand-accordion-from-url

function getBaseUrl() {
    let re = new RegExp(/^.*\//);
    let result = re.exec(window.location.href);
    return result;
}

// add base url prefix to direct job links
function setJobLinkBaseUrl() {
    $(".job-link").text(function (_, oldText) {
        return getBaseUrl() + oldText;
    });
}

function scrollTo(id) {
    let assumed_height = 130;
    let offset = $(id).offset().top - assumed_height;
    let body = $('html, body');
    body.animate({scrollTop: offset}, 2000);
}

function onCopyClick(container) {
    copyToClipboard(container);
    // $('#job_copy_confirm_popup').modal({
    //     closeExisting: true
    // });
    return false;
}

function copyToClipboard(container) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(container).text()).select();
    document.execCommand("copy");
    $temp.remove();
}

$(document).ready(function () {

    // replace the base-url of direct links
    setJobLinkBaseUrl();

    if (location.hash != null && location.hash !== "") {
        console.log("location.hash = " + location.hash);
        //scrollTo(location.hash);
        scrollTo(location.hash);
        setTimeout(function () {
            $('.collapse').removeClass('in');
            $(location.hash + '_content.collapse').collapse('show');
        }, 750);
    }
});